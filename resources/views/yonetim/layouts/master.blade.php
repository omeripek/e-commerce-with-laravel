<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="UTF-8">
    <title>@yield('title', config('app.name') . " | Yönetim Paneli")</title>
    @include('yonetim.layouts.partials.head')

    @yield('head')
</head>
<body>
@include('yonetim.layouts.partials.navbar')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-3 col-lg-2 sidebar collapse" id="sidebar">
            @include('yonetim.layouts.partials.sidebar')
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 main">
            @yield('content')
        </div>
    </div>
</div>
<script src='{{ asset('js/jquery-3.2.1.slim.min.js') }}'></script>
<script src='{{ asset('js/bootstrap.min.js') }}'></script>
<script src="{{ asset('js/admin-app.js') }}"></script>

@yield('footer')
</body>
</html>
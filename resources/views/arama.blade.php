@extends('layouts.master')
@section('content')
	<div class="container">
		<ol class="breadcrumb">
			<li>
				<a href="{{ route('anasayfa') }}">Anasayfa</a>
			</li>
			<li class="active">
				Arama Sonuçları
			</li>
		</ol>


		<div class="products bg-content">
			<div class="row">
				@if(count($urunler)==0)
					<div class="col-md-12 text-center">
						<div class="alert alert-warning" role="alert">
						  <h4 class="alert-heading">Arama Sonucu!</h4>

						  <hr>
						  <p class="mb-0">
						  		" {{ $aranan }} " Ürün Bulunamadı!
						  </p>

						  <hr>
						  <p>
						  	<a href="{{ route('anasayfa') }}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Anasayfaya Dön -> </a>
						  </p>
						</div>


					</div>
				@endif

				@foreach($urunler as $urun)
					<div class="col-md-3 product">
						<a href="{{ route('urun', $urun->slug) }}">
							<img src="//via.placeholder.com/250x200?text=UrunResmi" alt="{{ $urun->urun_adi }}" />
						</a>
						<p>
							 <a href="{{ route('urun', $urun->slug) }}">
							{{ $urun->urun_adi }}
							</a>
						</p>
						<p class="price">{{ $urun->fiyati }} ₺</p>
					</div>
				@endforeach
			</div>
			{{ $urunler->appends(['aranan' => old('aranan')])->links() }}
		</div>

	</div>

@endsection
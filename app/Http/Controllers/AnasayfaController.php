<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Urun;
use App\Models\UrunDetay;
use Illuminate\Http\Request;

class AnasayfaController extends Controller
{
    public function index() {

        $kategoriler = Kategori::whereRaw('ust_id is null')->take(8)->get();

        $urunler_slider = Urun::select('urun.*')
            ->join('urun_detay', 'urun_detay.urun_id', 'urun.id')
            ->where('urun_detay.goster_slider', 1)
            ->orderby('guncelleme_tarihi', 'desc')
            ->take(config('ayar.anasayfa_slider_urun_adet'))
            ->get();

        $urun_gunun_firsati = Urun::select('urun.*')
            ->join('urun_detay', 'urun_detay.urun_id', 'urun.id')
            ->where('urun_detay.goster_gunun_firsati', 1)
            ->orderby('guncelleme_tarihi', 'desc')
            ->first();

        $urunler_one_cikan = Urun::select('urun.*')
            ->join('urun_detay', 'urun_detay.urun_id', 'urun.id')
            ->where('urun_detay.goster_one_cikan', 1)
            ->orderby('guncelleme_tarihi', 'desc')
            ->take(config('ayar.anasayfa_liste_urun_adet'))
            ->get();

        $urunler_cok_satan = Urun::select('urun.*')
            ->join('urun_detay', 'urun_detay.urun_id', 'urun.id')
            ->where('urun_detay.goster_cok_satan', 1)
            ->orderby('guncelleme_tarihi', 'desc')
            ->take(get_ayar('anasayfa_liste_urun_adet'))
            ->get();

        $urunler_indirimli = Urun::select('urun.*')
            ->join('urun_detay', 'urun_detay.urun_id', 'urun.id')
            ->where('urun_detay.goster_indirimli', 1)
            ->orderby('guncelleme_tarihi', 'desc')
            ->take(config('ayar.anasayfa_liste_urun_adet'))
            ->get();

        //$urunler_one_cikan = UrunDetay::with('urun')->where('goster_one_cikan', 1)->take(4)->get();
        //$urunler_cok_satan = UrunDetay::with('urun')->where('goster_cok_satan', 1)->take(4)->get();
        //$urunler_indirimli = UrunDetay::with('urun')->where('goster_indirimli', 1)->take(4)->get();

        return view('anasayfa', compact('kategoriler', 'urunler_slider', 'urun_gunun_firsati', 'urunler_one_cikan', 'urunler_cok_satan', 'urunler_indirimli'));




/*//        return view('anasayfa', ['urun' => 'Elma']);
        $urun = "Elma";
        $fiyat = 7;
        $diziler = [1=>'sdfsd',2=>'rtryrty',3=>'olollo'];
        $kullanicilar = [
            ['id'=>1, 'kullanici' => 'Deneme'],
            ['id'=>2, 'kullanici' => 'Deneme 2'],
            ['id'=>3, 'kullanici' => 'Deneme 3'],
            ['id'=>4, 'kullanici' => 'Deneme 4']
        ];
       // return view('anasayfa')->with(['urun'=>$urun, 'fiyat'=>$fiyat]);

        return view('anasayfa', compact('urun', 'fiyat', 'diziler', 'kullanicilar'));*/
    }
}

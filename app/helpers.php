<?php

use App\Models\Ayar;
use Illuminate\Support\Facades\Cache;

if (! function_exists('get_ayar')) {
    function get_ayar($anahtar) {
        //$tumAyarlar = Ayar::all(); Her zaman veritabanından çek
        //$tumAyarlar = Cache::rememberForever('tumAyarlar', function (){ her zaman cache
        $dakika = 60; // 60 dakikada bir
        $tumAyarlar = Cache::remember('tumAyarlar', $dakika, function (){
            return Ayar::all();
        });

        return $tumAyarlar->where('anahtar', $anahtar)->first()->deger;
    }
}